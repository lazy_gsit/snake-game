// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class UBonusManagerComponent;
class AFood;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
	
	UPROPERTY(EditAnywhere)
	float MovementSpeed;
	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElement;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY(Blueprintable, BlueprintReadWrite)
	UBonusManagerComponent* BonusManager;
	
	AFood* Food;
	ASnakeElementBase* PrevElem;

	FVector PrevLocation;
	
	bool bCanMove;
	bool bCanDestroy = true;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
};
