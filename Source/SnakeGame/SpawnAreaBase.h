// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "GameFramework/Actor.h"
#include "SpawnAreaBase.generated.h"

class UBoxComponent;
class AFood;
class UKismetMathLibrary;
class ASnakeElementBase;

UCLASS()
class SNAKEGAME_API ASpawnAreaBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnAreaBase();

	UPROPERTY(EditAnywhere)
	UBoxComponent* BoxCollision;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(BlueprintReadWrite)
	AFood* Food;

	UPROPERTY(EditAnywhere)
	UKismetMathLibrary* KismetMathLibrary;

	UPROPERTY(BlueprintReadWrite)
	ASnakeElementBase* SnakeElement;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SpawnFood();

};
