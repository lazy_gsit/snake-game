// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Components/StaticMeshComponent.h"
#include "BonusManagerComponent.h"
// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;
	MovementSpeed = 0.5f;
	BonusManager = CreateDefaultSubobject<UBonusManagerComponent>(TEXT("Bonus Manager Component"));
	BonusManager->RegisterComponent();
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		for (int j = SnakeElement.Num() - 1; j > 0; --j)
		{
			PrevElem = SnakeElement[SnakeElement.Num() - 1];
			PrevLocation = PrevElem->GetActorLocation();
		}
		FTransform NewTransform = FTransform(PrevLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElement.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		bCanMove = false;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		bCanMove = false;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		bCanMove = false;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		bCanMove = false;
		break;
	}
	

	SnakeElement[0]->ToggleCollision();

	for (int i = SnakeElement.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = SnakeElement[i];
		auto PrevElement = SnakeElement[i - 1];
		FVector Prevlocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(Prevlocation);
	}

	SnakeElement[0]->AddActorWorldOffset(MovementVector);
	SnakeElement[0]->ToggleCollision();

	bCanMove = false;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIdex;
		SnakeElement.Find(OverlappedElement, ElemIdex);
		bool bIsFirst = ElemIdex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}


