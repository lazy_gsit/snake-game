// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BonusManagerComponent.generated.h"

class ASnakeBase;
class AFood;

UENUM(BlueprintType, meta = (ExposeOnSpawn = "true"))
enum class EBonusFood : uint8
{
	E_SPEEDUP		UMETA(DisplayName = "SPEED UP"),
	E_DOUBLEPOINT	UMETA(DisplayName = "DOUBLE POINT"),
	E_IMMORTALITY	UMETA(DisplayName = "IMMORTALITY"),
	MAX,
	NONE
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class SNAKEGAME_API UBonusManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBonusManagerComponent();

	UPROPERTY(BlueprintReadWrite, meta = (ExposeOnSpawn = "true"))
	EBonusFood BonusFood;
	ASnakeBase* OwnerSnake;
	
	FTimerHandle SpeedBoostTimerHandle;
	FTimerHandle ImmortalityTimerHandle;
	UPROPERTY(BlueprintReadOnly)
	FString CurrentBonusName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BonusActivationChance;
	UPROPERTY(EditAnywhere)
	float SpeedBoostDuration;
	UPROPERTY(EditAnywhere)
	float ImmortalityDuration;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Bonuses")
	EBonusFood GetRandomBonusType(float Chance);

	UFUNCTION(Blueprintable)
	void SpeedBoost(ASnakeBase* Snake);
	void SpeedBoostTimerExpired(ASnakeBase* Snake);
	UFUNCTION(Blueprintable)
	void DoublePoint(ASnakeBase* Snake);
	UFUNCTION(Blueprintable)
	void Immortality(ASnakeBase* Snake);
	void ImmortalityTimerExpired(ASnakeBase* Snake);
	UFUNCTION(Blueprintable)
	void ActivatingBonus(ASnakeBase* Snake);
};
