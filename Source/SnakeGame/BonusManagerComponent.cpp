// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusManagerComponent.h" 
#include "SnakeBase.h"
// Sets default values for this component's properties
UBonusManagerComponent::UBonusManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	BonusActivationChance = 1.f;
	SpeedBoostDuration = 10.f;
	ImmortalityDuration = 10.f;
}

void UBonusManagerComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UBonusManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

EBonusFood UBonusManagerComponent::GetRandomBonusType(float Chance)
{
	TArray<EBonusFood> AllBonuses = {EBonusFood::E_SPEEDUP, EBonusFood::E_DOUBLEPOINT, EBonusFood::E_IMMORTALITY};
	float RandomValue = FMath::FRand();
	int32 NumBonuses = static_cast<int32>(EBonusFood::MAX);
	int32 BonusIndex = FMath::FloorToInt(RandomValue * NumBonuses);

	if (BonusIndex >= AllBonuses.Num())
	{
		return EBonusFood::NONE;
	}
	if (FMath::FRand() < Chance)
	{
		return AllBonuses[BonusIndex];
	}
	return EBonusFood::NONE;
}

void UBonusManagerComponent::SpeedBoost(ASnakeBase* Snake)
{
	FTimerDelegate TimerCallBack;
	TimerCallBack.BindLambda([this, Snake]()
	{
		SpeedBoostTimerExpired(Snake);
	});
		
	GetWorld()->GetTimerManager().SetTimer(SpeedBoostTimerHandle, TimerCallBack, SpeedBoostDuration, false);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("SPEED UP"));
	Snake->SetActorTickInterval(0.1f);
}

void UBonusManagerComponent::SpeedBoostTimerExpired(ASnakeBase* Snake)
{
	Snake->SetActorTickInterval(0.5f);
	GetWorld()->GetTimerManager().ClearTimer(SpeedBoostTimerHandle);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("SPEED BOOSTT EXPIRED"));
}

void UBonusManagerComponent::DoublePoint(ASnakeBase* Snake)
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, TEXT("Double Point"));
	Snake->AddSnakeElement();
}

void UBonusManagerComponent::Immortality(ASnakeBase* Snake)
{
	FTimerDelegate TimerCallBack;
	TimerCallBack.BindLambda([this, Snake]()
	{
		ImmortalityTimerExpired(Snake);
	});
	
	GetWorld()->GetTimerManager().SetTimer(ImmortalityTimerHandle, TimerCallBack, ImmortalityDuration, false);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Immortality"));
	Snake->bCanDestroy = false;
}

void UBonusManagerComponent::ImmortalityTimerExpired(ASnakeBase* Snake)
{
	Snake->bCanDestroy = true;
	GetWorld()->GetTimerManager().ClearTimer(ImmortalityTimerHandle);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Immortality Expired"));
}

void UBonusManagerComponent::ActivatingBonus(ASnakeBase* Snake)
{
	if (Snake)
	{
		EBonusFood ActivatedBonus = GetRandomBonusType(BonusActivationChance);
		switch (ActivatedBonus)
		{
		case 0:
			SpeedBoost(Snake);
			break;
		case 1:
			DoublePoint(Snake);
			break;
		case 2:
			Immortality(Snake);
			break;
		}
	}
}



