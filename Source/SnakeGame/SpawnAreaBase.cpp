// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnAreaBase.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Food.h"
#include "SnakeElementBase.h"
// Sets default values
ASpawnAreaBase::ASpawnAreaBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collision"));
	KismetMathLibrary = CreateDefaultSubobject<UKismetMathLibrary>(TEXT("KismetMathLibrary"));
}

// Called when the game starts or when spawned
void ASpawnAreaBase::BeginPlay()
{
	Super::BeginPlay();
	SpawnFood();
}

// Called every frame
void ASpawnAreaBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpawnAreaBase::SpawnFood()
{
	if (FoodActorClass)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		FVector location = BoxCollision->GetComponentLocation();
		FVector scale = BoxCollision->GetScaledBoxExtent();
		FTransform SpawnTransform(KismetMathLibrary->RandomPointInBoundingBox(location, scale));

		Food = GetWorld()->SpawnActor<AFood>(FoodActorClass, SpawnTransform, SpawnParameters); 
		Food->OnDestroy.AddDynamic(this, &ASpawnAreaBase::SpawnFood);
	}
}

